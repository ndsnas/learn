package com.sapient.rt.model;

import java.util.List;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.sapient.rt.pojo.Location;
import com.sapient.rt.pojo.NearbyRestaurant;
import com.sapient.rt.pojo.Pojo;
import com.sapient.rt.pojo.R;
import com.sapient.rt.pojo.Restaurant;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Pojo restaurant = new Pojo();
		try {
			HttpResponse<JsonNode> response = Unirest.get("https://developers.zomato.com/api/v2.1/geocode?lat=28.7041&lon=77.1025")
					.header("user-key", "2d1a28a59f43a6399a5e9f82d3bf648a")
					.header("Accept", "application/json")
					.asJson();
			JSONObject myObj = response.getBody().getObject();
			String json = myObj.toString();
			Gson gson = new GsonBuilder().create();
			restaurant=gson.fromJson(json, Pojo.class);
			//System.out.println(restaurant.toString());
			//System.out.println(restaurant.toString());
			List<NearbyRestaurant> list = restaurant.getNearbyRestaurants();
			//System.out.println(list.get(1).getRestaurant().getName().toString());
			for(int i=0; i<list.size(); i++)
			{
				NearbyRestaurant nr = new NearbyRestaurant();
				nr = list.get(i);
				System.out.println(list.get(i).getRestaurant().getName().toString());
			}
				//System.out.println(list.get(i).getRestaurant().toString());
			//System.out.println(restaurant.getNearbyRestaurants().toString());
			//return restaurant;
			
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//return restaurant;

	}

}
