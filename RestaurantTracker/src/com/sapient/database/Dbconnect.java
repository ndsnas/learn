package com.sapient.database;

import java.sql.Connection;
import java.sql.DriverManager;

public class Dbconnect {

	public static Connection co = null;
	public static Connection getConnection() throws Exception {
	if(co==null) {	
		Class.forName("oracle.jdbc.driver.OracleDriver");
		co = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","sapient");
		System.out.println("Connection established");
	}
	
	return co;
	}
}
