import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class exer1 {
  static Logger log = Logger.getLogger("abc");
  public static void main(String args[])
  {

    List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5, 20, 22, 23);
    //Integer sum = integers.stream()
    //    .filter(number -> number%2==0)
    //    .reduce(0, (a, b) -> a + b);
    //System.out.println(sum);
    System.out.println(integers.stream().filter(num -> num %2==0).reduce(0, (a,b)->a+b));
    System.out.println(integers.stream().mapToInt(Integer::intValue).filter(num -> num %2 == 0).sum());

    log.info("hello");
  }

}
