package day2;

public class Controller {
	public static void main(String[] args) {
		AddBean ob = new AddBean();
		View vw = new View();
		vw.read(ob);
		AddDAO adddao = new AddDAO();
		adddao.compute(ob);
		vw.display(ob);
		
		ob=null;
		adddao=null;
		vw=null;
		//ob.finalize();
		
		Controller c = new Controller();
		c=null;
	}
	public void finalize()
	{
		System.out.println("finalize");
	}
}
