package lectures;


import beans.Car;
import java.math.BigDecimal;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import mockdata.MockData;
import org.junit.Test;

public class Lecture7 {

  @Test
  public void count() throws Exception {

    System.out.println(MockData.getPeople().stream()
        .filter(person -> person.getGender().equalsIgnoreCase("female"))
        .count());
  }

  @Test
  public void min() throws Exception {
    double min = MockData.getCars()
        .stream()
        .filter(car -> car.getColor().equalsIgnoreCase("yellow"))
        .mapToDouble(Car::getPrice)
        .min()
        .getAsDouble();
    System.out.println(min);

  }

  @Test
  public void max() throws Exception {

    double max = MockData.getCars()
        .stream()
        .filter(car -> car.getColor().equalsIgnoreCase("yellow"))
        .mapToDouble(Car::getPrice)
        .max()
        .getAsDouble();
    System.out.println(max);
  }


  @Test
  public void average() throws Exception {
    List<Car> cars = MockData.getCars();
    double avg = cars.stream()
        .mapToDouble(Car::getPrice)
        .average()
        .orElse(0.0);
    System.out.println(avg);

  }

  @Test
  public void sum() throws Exception {
    List<Car> cars = MockData.getCars();
    double sum = cars.stream()
        .mapToDouble(Car::getPrice)
        .sum();
    BigDecimal bigsum = BigDecimal.valueOf(sum);
    System.out.println(bigsum);


  }

  @Test
  public void statistics() throws Exception {
    List<Car> cars = MockData.getCars();
    DoubleSummaryStatistics stats = cars.stream()
        .mapToDouble(Car::getPrice)
        .summaryStatistics();
    System.out.println(stats);
    System.out.println(stats.getMax());
  }

}