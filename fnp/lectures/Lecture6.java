package lectures;


import java.util.Arrays;
import java.util.function.Predicate;
import org.junit.Test;

public class Lecture6 {

  //final Predicate<Integer> numbersLessThan10 = n -> n > 5 && n < 10;
  Predicate<Integer> numberLessThan10 = n -> n < 10 && n > 5;
  @Test
  public void findAny() throws Exception {
    Integer[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};


    Integer num = Arrays.stream(numbers)
        .filter(numberLessThan10)
        .findAny()
        .get();
    System.out.println(num);

  }

  @Test
  public void findFirst() throws Exception {
    Integer[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    Integer first = Arrays.stream(numbers)
        .filter(numberLessThan10)
        .findFirst()
        .get();

    System.out.println(first);
  }
}

