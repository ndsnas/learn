package com.sapient.login;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class EditServlet
 */
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	  // TODO Auto-generated method stub
    	  //response.getWriter().append("Served at: ").append(request.getContextPath());
    	  PrintWriter out = response.getWriter();
    	  String name = request.getParameter("id1");
    	  String name2 = request.getParameter("id2");
    	  String name3 = request.getParameter("id3");
    	  
    	  out.print("<html><body><center><form action='EditServlet' method='post'");
    	  
    	  out.print("<br>name <input type='text' name='a1' value='"+name+"'/>");
    	  out.print("<br>regno<input type='text' name='a2' value='"+name2+"'/>");
    	  out.print("<br>marks<input type='text' name='a3' value='"+name3+"'/>");
    	  out.print("<br><input type='submit' value='Update'/></form></body></html>");
    	 }
    	 /**
    	  * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
    	  */
    	 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	try {
    	           PrintWriter out = response.getWriter();
    	           String n1 = request.getParameter("a1");
    	   int n2 = Integer.parseInt(request.getParameter("a2"));
    	   int n3 = Integer.parseInt(request.getParameter("a3"));
    	   Connection co = Dbconnect.getConnection();
    	   PreparedStatement ps = co.prepareStatement("update student set name =?, marks =? where regno=?");
    	   ps.setString(1, n1);ps.setInt(2, n3);ps.setInt(3, n2);
    	   ResultSet rs = ps.executeQuery();
    	   
    	   response.sendRedirect("ListOfStudent");
    	   
    	   
    	   
    	  }
    	  catch(Exception e) {
    	   
    	   
    	  }
    	  
    	  
    	  
    	  
    	  
    	 }



}
