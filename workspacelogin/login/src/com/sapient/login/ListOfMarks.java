package com.sapient.login;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ListOfMarks
 */
public class ListOfMarks extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListOfMarks() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
		
		 
		 PrintWriter out = response.getWriter();
		  try {
		   
		   Connection co = Dbconnect.getConnection();
		   PreparedStatement ps = co.prepareStatement("select * from student");
		   ResultSet rs = ps.executeQuery();
		   
		   
		   out.print("<html><center><body><h1>List of Marks</h1>");
		   out.print("<table>" +
		     "  <tr>" +
		     "    <th>Firstname</th>" +
		     "    <th>Reg no.</th>" +
		     "    <th>Marks</th>" +
		     "  </tr>");
		    while (rs.next()) {
		                 String name = rs.getString(1);
		                 int id = rs.getInt(2);
		                 int marks = rs.getInt(3);                          
		               
		                 out.println("<tr><td>" + name + "</td><td>" + id + "</td><td>" + marks + "</td></tr>");
		             }
		     
		     
		     out.print("</table> ");
		   
		   out.print("</body></center></html>");
		   
		  }
		  catch(Exception e) {
		   
		   
		  }
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
