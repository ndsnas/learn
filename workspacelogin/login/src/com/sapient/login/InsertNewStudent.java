package com.sapient.login;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InsertNewStudent
 */
public class InsertNewStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertNewStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		PrintWriter out = response.getWriter();
		
		try {
			out.print("<html><center><body><h1>Insert New Student</h1>");
			   out.print("<form action = 'InsertNewStudent' method = 'post'>");
			   out.print("<br>Name<input type = 'text' name = 'name' value = '' />");
			   out.print("<br>RegNo<input type = 'text' name = 'regno' value = '' />");
			   out.print("<br>Marks<input type = 'text' name = 'Marks' value = '' />");
			   
			   
			   out.print("<br><input type = 'submit' value = 'insert' />");
			   
			   out.print("</form></body></center></html>");
		}
		
		catch (Exception e)
		{ 
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
PrintWriter out = response.getWriter();
		
		try {
			
			Connection con = Dbconnect.getConnection();
			System.out.println(request.getParameter("regno"));
			System.out.println(request.getParameter("Marks"));
		PreparedStatement ps = con.prepareStatement("insert into student values(?, ?, ?)");
		ps.setString(1, request.getParameter("name"));
		ps.setInt(2, Integer.parseInt(request.getParameter("regno")));
		ps.setInt(3, Integer.parseInt(request.getParameter("Marks")));
		ResultSet rs = ps.executeQuery();
			
			out.print("<html><center><body><h1>Insert New Student</h1>");
			   out.print("<form action = 'InsertNewStudent' method = 'post'>");
			   out.print("<br>Name<input type = 'text' name = 'name' value = '' />");
			   out.print("<br>RegNo<input type = 'text' name = 'regno' value = '' />");
			   out.print("<br>Marks<input type = 'text' name = 'Marks' value = '' />");
			   
			   
			   out.print("<br><input type = 'submit' value = 'insert' />");
			   out.print("<br>Record Updated!</br>");
			   out.print("</form></body></center></html>");
		}
		
		catch (Exception e)
		{ 
			
		}
		
		
	}

}
