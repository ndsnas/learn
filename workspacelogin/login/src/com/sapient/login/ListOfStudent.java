package com.sapient.login;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ListOfStudent
 */
public class ListOfStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListOfStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	 
    	  
    	  PrintWriter out = response.getWriter();
    	  try {
    	  //OPEN DB CONNECTION
    	  Connection con = Dbconnect.getConnection();
    	  //System.out.println("hello");
    	  PreparedStatement ps = con.prepareStatement("select * from student");
    	  ResultSet rs = ps.executeQuery();
    	  //rs.next();
    	  //out.print("<html><body><h1>helo</h1></body></html>");
    	  out.print("<table width=25% border=1>");
    	          out.print("<center><h1>Result:</h1></center>");
    	          out.print("<th>Name</th>");
    	          out.print("<th>RegNo</th>");
    	          out.print("<th>Marks</th>");
    	          out.print("<th>Operations</th>");
    	  /* Printing column names */
    	          //ResultSetMetaData rsmd=rs.getMetaData();
    	         
    	          while(rs.next())
    	          {
    	          out.print("<tr>");
    	          
    	          String name = rs.getString(1);
    	                int id = rs.getInt(2);
    	                int marks = rs.getInt(3);
    	               
    	          out.print("<td >"+name+"</td>");
    	          out.print("<td>"+id+"</td>");
    	          out.print("<td>"+marks+"</td>");
    	         out.print("<td><a href=EditServlet?id1="+name+"&id2="+id+"&id3="+marks+">edit</a></td>");
    	         out.print("<td><a href=DeleteServlet?id1="+name+"&id2="+id+"&id3="+marks+">delete</a></td>");
    	      //    out.print("<td><input type = 'submit' value = 'Edit' /><input type = 'submit' value = 'Delete' /></td>");
    	         
    	                          
    	       }
    	       out.print("</table>");
    	         
    	         
    	  }
    	  catch(Exception e)
    	  {
    	  }
    	 }
    	 /**
    	  * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
    	  */
    	 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	  // TODO Auto-generated method stub
    	  //doGet(request, response);
    	  
    	  try {String n1 = request.getParameter("a");
    	   System.out.println("Hello");
    	   
    	   System.out.println(n1);
    	   request.setAttribute("a",n1);
    	   RequestDispatcher rd = request.getRequestDispatcher("EditServlet");
    	   rd.forward(request,response);
    	   
    	  
    	  
    	   
    	 }catch(Exception e) {
    	  
    	  
    	 }
    	}

}
