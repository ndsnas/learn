package com.sapient.login;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DeleteServlet
 */
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter out = response.getWriter();
  	  String name = request.getParameter("id1");
  	  String name2 = request.getParameter("id2");
  	  String name3 = request.getParameter("id3");
  	  
  	  out.print("<html><body><center><form action='DeleteServlet' method='post'");
  	  
  	  out.print("<br>name <input type='text' name='a1' value='"+name+"'/>");
  	  out.print("<br>regno<input type='text' name='a2' value='"+name2+"'/>");
  	  out.print("<br>marks<input type='text' name='a3' value='"+name3+"'/>");
  	  out.print("<br><input type='submit' value='Delete'/></form></body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		try {
	           PrintWriter out = response.getWriter();
	           String n1 = request.getParameter("a1");
	   int n2 = Integer.parseInt(request.getParameter("a2"));
	   int n3 = Integer.parseInt(request.getParameter("a3"));
	   Connection co = Dbconnect.getConnection();
	   PreparedStatement ps = co.prepareStatement("delete from student where regno=?");
	   ps.setInt(1, n2);
	   ResultSet rs = ps.executeQuery();
	   
	   response.sendRedirect("ListOfStudent");
	   
	   
	   
	  }
	  catch(Exception e) {
	   
	   
	  }
	}

}
