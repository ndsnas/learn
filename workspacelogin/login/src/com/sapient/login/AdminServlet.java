package com.sapient.login;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdminServlet
 */
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		try {
		
			out.print("<html><center><body><h1>Admin Page</h1>");
			//out.print("<form action = 'AdminServlet' method = 'post'>");
			out.print("<br><select action = 'AdminServlet' method = 'post'><option value='insert'>Insert</option>");
			out.print("<option value='list'>List</option>");
			out.print("</select>");
			
			out.print("<br><select action = 'AdminServlet' method = 'post'><option value='marks'>Marks</option>");
			out.print("</select>");
			
			
			out.print("</body></center></html>");
	}

		catch (Exception e)
		{
			}
		}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
