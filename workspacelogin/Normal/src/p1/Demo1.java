package p1;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

public class Demo1 {
	
	public static void main(String[] args) {
		ClientConfig config = new ClientConfig();//open config class
		Client client = ClientBuilder.newClient(config);//create a client
		WebTarget target = client.target(getBaseUri());//target where to go
		System.out.println(target.path("rest").path("hello").request().accept(MediaType.TEXT_PLAIN).get(String.class));
	}
private static URI getBaseUri() {
	return UriBuilder.fromUri("http://10.151.61.73:8081/DemoREST").build();
}
}
