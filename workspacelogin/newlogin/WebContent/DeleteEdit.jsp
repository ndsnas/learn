<%@page import="com.sapient.student.bean.StudentBean"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<%
Map<String,StudentBean> map = (Map<String,StudentBean>)config.getServletContext().getAttribute("smlist");
%>
<body>
<center>
<h1>
Edit Delete Module
</h1>
<table border = "1">
<%
for(StudentBean ob : map.values()){
if(ob.getRollno().equals("REGNO")){
	continue;
}
%>
<tr>
<td><%=ob.getRollno() %></td><td><%=ob.getName() %></td><td><%=ob.getMarks() %></td>
<td><a href=Edit.jsp?id1=<%=ob.getRollno()%>&id2=<%=ob.getName()%>&id3=<%=ob.getMarks()%>>Edit</a></td>
<td><a href=Delete.jsp?id1=<%=ob.getRollno()%>&id2=<%=ob.getName()%>&id3=<%=ob.getMarks()%>>Delete</a></td>
</tr>
<%
}%>
</table>
</center>
</body>
</html>