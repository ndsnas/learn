package com.sapient.admin.controller;

import java.util.Map;

import javax.servlet.ServletContext;

import com.sapient.student.bean.StudentBean;

public class AdminDAO {

	public void studentUpdate(ServletContext cn, StudentBean sb)
	{
		Map<String, StudentBean> map = ((Map<String, StudentBean>)cn.getAttribute("smlist"));
		map.put(sb.getRollno(), sb);
		cn.setAttribute("smlist", map);
		
	}
}
