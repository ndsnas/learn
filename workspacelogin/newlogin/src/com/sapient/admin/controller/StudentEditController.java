package com.sapient.admin.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.student.bean.StudentBean;

/**
 * Servlet implementation class StudentEditController
 */
public class StudentEditController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentEditController() {
        super();
        // TODO Auto-generated constructor stub
    }

    ServletContext cn;


    @Override
    public void init(ServletConfig config) throws ServletException {
    	// TODO Auto-generated method stub
    	super.init(config);
    	cn=config.getServletContext();
    	
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String rollno = request.getParameter("id1");
		String name = request.getParameter("id2");
		String marks = request.getParameter("id3");
		StudentBean sb = new StudentBean(name, rollno, marks);
		AdminDAO adao = new AdminDAO();
		adao.studentUpdate(cn, sb);
		RequestDispatcher rd = request.getRequestDispatcher("/DeleteEdit.jsp");
 	   rd.forward(request,response);
		//cn.set
	}

}
