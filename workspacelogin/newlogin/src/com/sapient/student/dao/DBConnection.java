package com.sapient.student.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	static Connection con = null;
	public static Connection getConnection() throws Exception{
		
		try {
			
			if(con==null)
			{
				Class.forName("oracle.jdbc.driver.OracleDriver");
				con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","sapient"); 
				con.setAutoCommit(true);
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return con;
	}

}
