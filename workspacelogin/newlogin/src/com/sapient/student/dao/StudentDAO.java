package com.sapient.student.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Map;
import java.util.TreeMap;

import com.sapient.student.bean.StudentBean;

public class StudentDAO {
	
	public Map<String, StudentBean> getAllStudent() throws Exception{
		
		Connection con = DBConnection.getConnection();
		Map<String, StudentBean> map = new TreeMap<String, StudentBean>();
		PreparedStatement ps = con.prepareStatement("select * from student");
		ResultSet rs = ps.executeQuery();
		ResultSetMetaData meta = rs.getMetaData();
		map.put("column_name", new StudentBean(meta.getColumnName(1), meta.getColumnName(2), meta.getColumnName(3)));
		while(rs.next())
		{
			String name = rs.getString(1);
			String rollno = rs.getString(2);
			String marks = rs.getString(3);
			System.out.println(marks);
			map.put(rollno, new StudentBean(name, rollno, marks));
		}
		return map;
	}

}
