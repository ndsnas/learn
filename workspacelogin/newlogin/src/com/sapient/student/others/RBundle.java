package com.sapient.student.others;

import java.util.ResourceBundle;

public class RBundle {

	public static String getErrorValues(String key)
	{
		ResourceBundle rb = ResourceBundle.getBundle("student");
		return rb.getString(key);
	}
}
