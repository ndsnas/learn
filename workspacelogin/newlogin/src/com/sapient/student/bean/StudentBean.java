package com.sapient.student.bean;

public class StudentBean {
	
	private String rollno;
	private String name;
	private String marks;
	
	public StudentBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public StudentBean(String name, String rollno, String marks) {
		super();
		this.rollno = rollno;
		this.name = name;
		this.marks = marks;
	}

	@Override
	public String toString() {
		return "StudentBean [rollno=" + rollno + ", name=" + name + ", marks=" + marks + "]";
	}

	public String getRollno() {
		return rollno;
	}
	public void setRollno(String rollno) {
		this.rollno = rollno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMarks() {
		return marks;
	}
	public void setMarks(String marks) {
		this.marks = marks;
	}
	

}
