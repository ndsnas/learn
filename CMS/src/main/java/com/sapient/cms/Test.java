package com.sapient.cms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public  class Test {
	//static final String URL_CREATE_EMPLOYEE = "http://localhost:8000/cms/register";
	@Autowired
	CourseDetailDAO cdao;
	
	@org.junit.Test
	public void test2IfIdNotPresent() { //delete testing
		String URL_CREATE_EMPLOYEE = "http://localhost:8001/cms/delete";
		long id = 1; //put a non-existing id here!
		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    RestTemplate restTemplate = new RestTemplate();
	    HttpEntity<Long> requestBody = new HttpEntity<>(id, headers);
	    String e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, String.class);
	
        
        System.out.println(e);
        assertEquals("id not present!", e);
       
	}
	@org.junit.Test
	public void test2IfIdPresent() {  //delete testing
		String URL_CREATE_EMPLOYEE = "http://localhost:8001/cms/delete";
		long id = 25; //put an existing id here!
		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    RestTemplate restTemplate = new RestTemplate();
	    HttpEntity<Long> requestBody = new HttpEntity<>(id, headers);
	    String e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, String.class);
	
        
        System.out.println(e);
        assertEquals("Course Deleted!", e);
       
	}
	
	@org.junit.Test
	public void testSearchIfIdPresent() { //search testing
		String URL_CREATE_EMPLOYEE = "http://localhost:8001/cms/search";
		long id = 26; //put an existing id!
		CourseDetail e;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    RestTemplate restTemplate = new RestTemplate();
	    HttpEntity<Long> requestBody = new HttpEntity<>(id, headers);
	    e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, CourseDetail.class);
	
        
       // System.out.println(e);
        //assertEquals("Course Deleted!", e);
       assertNotNull(e);
	}
	@org.junit.Test
	public void testSearchIfIdNotPresent() { //search testing
		String URL_CREATE_EMPLOYEE = "http://localhost:8001/cms/search";
		long id = 2; //put a non existing id!
		CourseDetail e;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    RestTemplate restTemplate = new RestTemplate();
	    HttpEntity<Long> requestBody = new HttpEntity<>(id, headers);
	    e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, CourseDetail.class);
	
        
       // System.out.println(e);
        //assertEquals("Course Deleted!", e);
       assertNull(e);
	}
	@org.junit.Test
	public void testRegister() {
	String URL_CREATE_EMPLOYEE = "http://localhost:8001/cms/register";
	CourseDetail ob = new CourseDetail("a", "b", "c", "d");
	HttpHeaders headers = new HttpHeaders();
	headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
    headers.setContentType(MediaType.APPLICATION_JSON);
    RestTemplate restTemplate = new RestTemplate();
    HttpEntity<CourseDetail> requestBody = new HttpEntity<>(ob, headers);
    String e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, String.class);
    
    
    System.out.println(e);
    assertEquals("Course Saved!", e);
}

}
