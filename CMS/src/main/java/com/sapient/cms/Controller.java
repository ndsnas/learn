package com.sapient.cms;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
@RequestMapping("/cms")
public class Controller {

	@Autowired
	CourseDetailDAO cdao;
	
	@PostMapping("/register")
	public String register(@RequestBody CourseDetail ob) {
		
		cdao.save(ob);
		return "Course Saved!";
	}
	@GetMapping("/list")
	public List<CourseDetail> list() {
		return (List<CourseDetail>) cdao.findAll();
	}
	@PostMapping("/delete")
	public String delete(@RequestBody long id) {
		//System.out.println(cdao.findById(id));
		if(cdao.existsById(id)) {
			cdao.deleteById(id);
			return "Course Deleted!";
		}
			return "id not present!";
		
	}
	
	@PostMapping("/search")
	public Optional<CourseDetail> search(@RequestBody long id) {
		//return cdao.findById(id);
		if(cdao.existsById(id)) {
			return cdao.findById(id);
			//return "Course Deleted!";
		}
			return null;
	}
}
