package com.sapient.cms;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CourseDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	@Column
	private String title;
	@Column
	private String csd;
	@Column
	private String ced;
	@Column
	private String fee;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCsd() {
		return csd;
	}
	public void setCsd(String csd) {
		this.csd = csd;
	}
	public String getCed() {
		return ced;
	}
	public void setCed(String ced) {
		this.ced = ced;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	public Long getId() {
		return id;
	}
	public CourseDetail(String title, String csd, String ced, String fee) {
		super();
		this.title = title;
		this.csd = csd;
		this.ced = ced;
		this.fee = fee;
	}
	public CourseDetail() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
}
