package com.sapient.cms;

import org.springframework.data.repository.CrudRepository;

public interface CourseDetailDAO extends CrudRepository<CourseDetail, Long>{
	
	//public CourseDetail findByName(String name);

}
