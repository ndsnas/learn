package client_side;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

public class Client_file {
	public static void main(String[] args) {
			ClientConfig config = new ClientConfig();
			Client client = ClientBuilder.newClient(config);
			WebTarget target = client.target(getBaseUri());
			//MediaType m[] = new MediaType[]{MediaType.TEXT_HTML_TYPE};
			System.out.println(target.path("rest").path("hello").request().accept(MediaType.TEXT_PLAIN).get(String.class));

			
	}

	private static URI getBaseUri() {
		// TODO Auto-generated method stub
		return UriBuilder.fromUri("http://localhost:9090/DemoJSP").build();
	}

}
