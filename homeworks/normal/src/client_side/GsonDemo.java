package client_side;



import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonDemo {

	public static void main(String[] args) {
		String json ="{\"city\":\"Jos\",\"country\":\"Nigeria\",\"houseNumber\":\"13\",\"lga\":\"Jos South\",\n" + 
				"\"state\":\"Plateau\",\"streetName\":\"Jonah Jann\",\"village\":\"Bukuru\",\"ward\":\"1\"}";
				Gson gson = new GsonBuilder().create();
				Address address=gson.fromJson(json, Address.class);
				System.out.println(address.getCity());
	}
}
