package com.sapient.studentController;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.studentModel.StudentDAO;
import com.sapient.studentPOJO.StudentBean;

/**
 * Servlet implementation class StudentDeleteServletController
 */
public class StudentDeleteServletController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentDeleteServletController() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    ServletContext cn;
    @Override
    public void init(ServletConfig config) throws ServletException {
    	// TODO Auto-generated method stub
    	super.init(config);
    	cn = config.getServletContext();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String name = request.getParameter("name");
		String rollno = request.getParameter("rollno");
		String marks = request.getParameter("marks");
		StudentBean sb = new StudentBean(name, rollno, marks);
		StudentDAO sdao = new StudentDAO();
		sdao.delete(sb, cn);
		response.sendRedirect("EditDelete.jsp");
	}

}
