package com.sapient.student;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbConnect {
	private static final String url = "jdbc:mysql://localhost:3306/test";
	 
    private static final String user = "root";
 
    private static final String password = "root";
    
    public static Connection con = null;
    
    public static Connection getConnection() throws Exception{
    	if(con==null)
    	{
    		Class.forName("com.mysql.jdbc.Driver"); 
    		con = DriverManager.getConnection(url, user, password);
            System.out.println("Success");
    	}
    	return con;
    }
 
    

}
