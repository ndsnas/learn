package com.sapient.nbaModel;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
//import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.sapient.nba.pojo.NBAPojo;



public class NbaDAO {
	
	public NBAPojo getDetails(String pid)
	{
		NBAPojo nbapojo = new NBAPojo();
		try {
			HttpResponse<JsonNode> response = Unirest.get("https://free-nba.p.rapidapi.com/players/"+pid)
					.header("X-RapidAPI-Host", "free-nba.p.rapidapi.com")
					.header("X-RapidAPI-Key", "148997fdb9msh306da9db9fa6d1ep17e0a8jsne658e8c1cfa9")
					.asJson();
			JSONObject myObj = response.getBody().getObject();
			String json = myObj.toString();
			Gson gson = new GsonBuilder().create();
			nbapojo=gson.fromJson(json, NBAPojo.class);
			System.out.println(nbapojo.getFirstName());
			return nbapojo;
			
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return nbapojo;
		
	}
	
}
