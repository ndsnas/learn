package com.sapient.studentPOJO;

public class StudentBean {
	private String name;
	private String rollno;
	private String marks;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRollno() {
		return rollno;
	}
	public void setRollno(String rollno) {
		this.rollno = rollno;
	}
	public String getMarks() {
		return marks;
	}
	public void setMarks(String marks) {
		this.marks = marks;
	}
	public StudentBean(String name, String rollno, String marks) {
		super();
		this.name = name;
		this.rollno = rollno;
		this.marks = marks;
	}

}
