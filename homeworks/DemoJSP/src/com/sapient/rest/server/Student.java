package com.sapient.rest.server;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.io.*;

import javax.swing.text.html.HTMLDocument.HTMLReader.PreAction;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.sapient.student.DbConnect;



@Path("/hello")
public class Student {

	@GET  
	@Produces(MediaType.TEXT_PLAIN)  	  
	public String getString1(){
		return "Welcome to CTS ACADEMY BY VIJAYA. INTRODUCTION TO REST";
	}
	@GET
	@Path("/inset/{name}/{rollno}/{marks}")
	public String insert(@PathParam("name") String name, @PathParam("rollno") String rollno, @PathParam("marks") String marks) {
		Connection con;
		
		try {
			con = DbConnect.getConnection();
			PreparedStatement ps = con.prepareStatement("insert into student values(?, ?, ?)");
			ps.setString(1, name);
			ps.setString(2, rollno);
			ps.setString(3, marks);
			int i = ps.executeUpdate();
			return "inserted";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "problem";
		
	}
	 
}