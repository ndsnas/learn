package com.sapient.studentModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;

import com.sapient.student.DbConnect;
import com.sapient.studentPOJO.StudentBean;

public class StudentDAO {
	public Map<String, StudentBean> getAllStudent() throws Exception{
		Map<String, StudentBean> map = new TreeMap<String, StudentBean>();
		
		Connection con = DbConnect.getConnection();
		PreparedStatement ps = con.prepareStatement("select * from student");
		ResultSet rs = ps.executeQuery();
		ResultSetMetaData meta = rs.getMetaData();
		map.put("column_name", new StudentBean(meta.getColumnName(1), meta.getColumnName(2), meta.getColumnName(3)));
		while(rs.next())
		{
			String name = rs.getString(1);
			String rollno = rs.getString(2);
			String marks = rs.getString(3);
			map.put(rollno, new StudentBean(name, rollno, marks));
			//System.out.println(name);
		}
		
		return map;
	}
	
	public void edit(StudentBean sb, ServletContext cn) {
		
		Map<String, StudentBean> map = (Map<String, StudentBean>) cn.getAttribute("smlist");
		map.put(sb.getRollno(), sb);
		cn.setAttribute("smlist", map);
		
	}

	public void delete(StudentBean sb, ServletContext cn) {
		// TODO Auto-generated method stub
		Map<String, StudentBean> map = (Map<String, StudentBean>) cn.getAttribute("smlist");
		map.remove(sb.getRollno());
		cn.setAttribute("smlist", map);
		
	}

}
