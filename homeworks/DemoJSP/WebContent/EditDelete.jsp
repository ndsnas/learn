<%@page import="java.util.Map"%>
<%@page import="com.sapient.studentPOJO.StudentBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>EditDelete</title>
</head>
<%
Map<String, StudentBean> map = (Map<String, StudentBean>)config.getServletContext().getAttribute("smlist");
%>
<body>

<center>
<table border="1">
<%
for(StudentBean sb : map.values())
{
	if(sb.getName().equals("name"))
		continue;
%>
<tr><td><%=sb.getName() %></td><td><%=sb.getRollno() %></td><td><%=sb.getMarks() %></td>
<td><a href=Edit.jsp?name=<%=sb.getName() %>&rollno=<%=sb.getRollno() %>&marks=<%=sb.getMarks() %>>Edit</a></td>
<td><a href=Delete.jsp?name=<%=sb.getName() %>&rollno=<%=sb.getRollno() %>&marks=<%=sb.getMarks() %>>Delete</a></td>
</tr>
<%
}
%>
</table>
</center>

</body>
</html>