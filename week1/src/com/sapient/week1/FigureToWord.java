package com.sapient.week1;

public class FigureToWord {

	public static String getWords(long amt)
	{
		String word = "";
		String[] unit = {"", "One", "Two", "Three", "Four",
				"Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve",
				"Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen",
	"Eighteen", "Nineteen"};
		String[] tens = {"", 		// 0
				"Ten",		// 1
				"Twenty", 	// 2
				"Thirty", 	// 3
				"Forty", 	// 4
				"Fifty", 	// 5
				"Sixty", 	// 6
				"Seventy",	// 7
				"Eighty", 	// 8
	"Ninety"};
		String[] vunit = {"crores", "lakhs", "thousands", "hundreds", "only"};
		long[] nunit = {10000000L, 100000L, 1000L, 100L, 1};
		
		for(int i=0; i<nunit.length; i++)
		{
			int n = (int)(amt/nunit[i]);
			amt = amt%nunit[i];
			if(n>0)
			{
				if(n>19)
				{
					word = word + tens[n/10] + unit[n%10]+ vunit[i];
				}
				else
				{
					word = word + unit[n] + vunit[i];
				}
			}
		}
		
		
		return word;
	}

	

}
