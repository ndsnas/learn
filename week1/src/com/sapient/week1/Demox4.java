package com.sapient.week1;

public class Demox4 {
	public static void main(String[] args) {
		matrix ob1 = new matrix();
		ob1.read();
		matrix ob2 = new matrix(new int[][] {{1, 2, 3},{4, 5, 6},{7, 8, 9}});
		matrix ob3 = ob1.add(ob2);
		ob3.display();
		
	}

}
