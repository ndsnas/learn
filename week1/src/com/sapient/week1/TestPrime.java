package com.sapient.week1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestPrime {

	@Test
	void test() {
		
		assertEquals(true, Prime.isPrime(13));
		assertEquals(true, Prime.isPrime(19));
		assertEquals(false, Prime.isPrime(18));
	}

}
